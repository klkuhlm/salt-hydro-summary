import numpy as np
import matplotlib.pyplot as plt

d = np.loadtxt('permeability-data.dat',skiprows=1,delimiter=',',usecols=(1,2,3,5,6))
d[d == -99] = np.NaN

# masks for different locations & symbols
m2 = np.logical_and(d[:,2] == 50,d[:,0] > 0)
m4 = np.logical_and(d[:,2] == 100,d[:,0] > 0)
m8 = np.logical_and(d[:,2] == 200,d[:,0] > 0)

fig = plt.figure(1,figsize=(10,5))
ax1 = fig.add_subplot(111)
ax1.semilogy(d[m2,0]+0.5,d[m2,3],'k*',label='B2')
ax1.semilogy(d[m4,0]+0.5,d[m4,3],'ro',label='B4')
ax1.semilogy(d[m8,0]+0.5,d[m8,3],'g^',label='B8')
ax1.set_xlim([326,343])

ax1.set_xticklabels(ax1.get_xticklabels(),size='large')
ax1.set_xlabel('heating days',size='x-large')
ax1.set_ylabel('log$_{10}$ permeability [m$^2$]',size='x-large')
ax1.set_yticklabels(['%i' % (np.log10(x),) for x in ax1.get_yticks()],size='large')

ax1.legend(loc=4,numpoints=1)

ax2 = ax1.twinx()
ax2.bar(d[m2,0],d[m2,1],width=1.0,alpha=0.25,color='red')
ax2.set_yticklabels(['%i' % (x*9,) for x in ax2.get_yticks()],size='large')
ax2.set_ylabel('heater power [W]',size='x-large')
ax2.set_xlim([326,343])

plt.savefig('avery-island-permeability-during-cooling.pdf')
