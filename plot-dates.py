import numpy as np
import matplotlib.dates as md
import matplotlib.pyplot as plt

dn2s = md.datestr2num
dt = np.dtype([('loc','S15'),('test','S50'),('begin','f8'),('end','f8'),('y','f8')])

d = np.loadtxt('test-dates.csv',skiprows=1,delimiter=',',dtype=dt,converters={2:dn2s,3:dn2s})

fig = plt.figure(1,figsize=(11,4))
ax = fig.add_subplot(111)

colors = {'Avery Island':'red','MCC potash mine':'green',
          'laboratory':'blue','WIPP':'cyan',
          'Asse':'magenta','Salt Vault':'purple'} #,'black'}

for e in d:
    ax.plot_date([e['begin'],e['end']],
                 [e['y'],e['y']],'k-',
                 color=colors[e['loc']],linewidth=3.0)

ax.set_xlim([dn2s('1/1/1960'),dn2s('1/1/2010')])
ax.set_ylim([0.85,5.25])
ax.set_yticks([],[])
ax.xaxis.set_minor_locator(md.YearLocator())

plt.savefig('timeline.pdf')
plt.close(1)
