import mpmath as mp
import matplotlib.pyplot as plt

# mctigue solution for flow to isothermal borehole
# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

# define shortcuts
J0 = lambda x : mp.besselj(0,x)
Y0 = lambda x : mp.bessely(0,x)

ep = 1.0E-8
beta = mp.exp(mp.euler)

if 1:
    integrand = lambda x : (mp.exp(-tD*x**2)/(x*(J0(x)**2 + Y0(x)**2))*
                            (J0(x*rD)*Y0(x) - Y0(x*rD)*J0(x)))
    i2 = lambda x: mp.exp(-tD*x**2)/(x*mp.ln(beta*x/2)**2)

    # compute for fixed tD and range of rD
    #tDv = [0.01,0.1,0.5,1.0,5.0,10.0,50.0]
    tDv = [0.01,0.1] ##,1.0,10.0]
    nr = 15
    rDv = mp.linspace(1.0,5.0,nr)
    pD = mp.matrix(nr,1)
    plt.figure(2)

    print 'pD(rD)'
    for tD in tDv:
        for j in range(nr):
            rD = rDv[j]
            # McTigue's analytical integration of singular part
            #pD[j] = (-mp.exp(-ep**2*tD)*mp.ln(rD)/mp.ln(beta*ep/2.0) -
            #          2/mp.pi * mp.quad(integrand,[ep,mp.inf],maxorder=10))
            
            pD[j] = -2.0/mp.pi*mp.quad(integrand,[0,mp.inf],maxorder=10)
            print j,tD,rD,pD[j]
        plt.plot(rDv,pD,label='t=%.2g' % tD)    

    plt.xlabel('$r_D$')
    plt.ylabel('$p_D$')
    plt.ylim([0,1])
    plt.legend(loc=0)
    plt.savefig('mctigue-isothermal.pdf')
    plt.close(2)


# mctigue solution for flow to heated borehole
# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

J1 = lambda x : mp.besselj(1,x)
Y1 = lambda x : mp.bessely(1,x)

if 0:
    
    def tempint(x):
        J1x = J1(x)
        Y1x = Y1(x)
        denom = x**2*(J1x**2 + Y1x**2)
        return (-mp.expm1(-tD*x**2)/denom *
                 (J0(x*rD)*Y1x - Y0(x*rD)*J1x))

    def presint(x):
        J1x = J1(x)
        Y1x = Y1(x)
        denom = x**2*(J1x**2 + Y1x**2)

        psi1 = -2/(mp.pi*x)
        psi2 = J0(x)*J1x + Y0(x)*Y1x

        J0ax = J0(rtalD*x)
        Y0ax = Y0(rtalD*x)

        phi1 = J0ax*psi1 + Y0ax*psi2
        phi2 = J0ax*psi2 - Y0ax*psi1

        return (-mp.expm1(-tD*x**2)/denom *
                 (phi1*J0(x*rtalD*rD) - phi2*Y0(x*rtalD*rD))/
                 (J0ax**2 + Y0ax**2))

        
    # compute for fixed tD and range of rD
    tDv = [0.1,1.0,10.0]
    nr = 30
    nt = len(tDv)
    rDv = mp.linspace(1.0,5.0,nr)
    thetaD = mp.matrix(nr,nt)
    pD = mp.matrix(nr,nt)
    alphaD = 1.5  # hydro time scale / thermal time scale
    rtalD = mp.sqrt(alphaD)

    for i in range(nt):
        for j in range(nr):
            rD = rDv[j]
            tD = tDv[i]
            # thermal solution
            thetaD[j,i] = -2.0/mp.pi*mp.quad(tempint,[0,mp.inf],maxorder=12)
            with mp.extradps(15):
                # integral in pressure solution
                tmp = -2.0/mp.pi*mp.quad(presint,[0,mp.inf],maxorder=12)
                # pressure solution
                pD[j,i] = alphaD/(alphaD-1)*(thetaD[j,i] - tmp)
                
            print j,i,rD,tD,thetaD[j,i],pD[j,i]

    plt.figure(2,figsize=(11,8))
    plt.subplot(211)
    for i in range(nt):
        plt.plot(rDv,thetaD[:,i],label='t=%.2g' % tDv[i])    
    plt.xlabel('$r_D$')
    plt.ylabel('$\\theta_D$')
    plt.legend(loc=0)

    plt.subplot(212)
    for i in range(nt):
        plt.plot(rDv,pD[:,i],label='t=%.2g' % tDv[i])    
    plt.xlabel('$r_D$')
    plt.ylabel('$p_D$')
    plt.legend(loc=0)
    
    plt.savefig('mctigue-thermoporoelasticity.pdf')
    plt.close(2)
