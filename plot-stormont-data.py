import numpy as np
import matplotlib.pyplot as plt

fn = 'stormont-1991-data.csv'
fh = open(fn,'r')
lines = fh.readlines()[1:]
fh.close()

## borehole,fluid,position,perm (m^2),porosity

bd = []
gd = []
for line in lines:
    f = line.split(',')
    if 'brine' in f[1]:
        bd.append([float(x) for x in f[2:]])
    else:
        gd.append([float(x) for x in f[2:]])

bd = np.array(bd)
gd = np.array(gd)

r = np.linspace(1.0,5.0)
k0 = 1.0E-15
k1 = 5.0E-21
alpha = -20.0
kk = k1 + k0*r**alpha

fig = plt.figure(1)
fig.subplots_adjust(hspace=0.05, wspace=0.05)
axk = fig.add_subplot(211)
axk.semilogy(bd[:,0],bd[:,1],'ko',label='brine')
axk.semilogy(gd[:,0],gd[:,1],'rx',label='gas')
axk.semilogy(r,kk,'g--',label='$k=5\\times10^{-21} + 10^{-15} R^{-20}$')
axk.set_ylabel('Permeability [m$^2$]')
axk.set_xlim([1,4.5])
axk.set_ylim([1.0E-21,1.0E-14])
axk.grid()
axk.legend(loc=0)
axk.set_xticklabels([])

p0 = 0.03
p1 = 0.001
axp = fig.add_subplot(212)
axp.semilogy(bd[:,0],bd[:,2],'ko')
axp.semilogy(gd[:,0],gd[:,2],'rx')

#beta = -6.5
#pp = p1 + p0*r**beta
#axp.semilogy(r,pp,'b--',label='$\\phi=\\phi_1+\\phi_1 R^\\beta$')

axp.set_ylabel('Porosity $\\phi$')
axp.set_xlabel('$R=r/r_0$')
axp.set_xlim([1,4.5])
axp.set_ylim([0.0003,0.03])
axp.grid()
#axp.legend(loc=0)

plt.savefig('small-scale-mineby-results-model.pdf')

